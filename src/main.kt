import arabiannights.Genie
import arabiannights.MagicLamp
import arabiannights.RecyclableDemon

fun main() {

    val totalRubs = 5
    val lampRubCapacity = 2

    val wishesPerGenie = 4
    val genieWishCapacity = 3

    val magicLamp = MagicLamp(lampRubCapacity)
    var lastGenie : Genie

    for(rub in 0 until totalRubs) {

        lastGenie = magicLamp.rub(genieWishCapacity)

        for (wish in 0 until wishesPerGenie) {
            val wishGranted = lastGenie.grantWish()
            if (wishGranted) println("${lastGenie.name} granted a wish.")
            else println("${lastGenie.name} did not grant you your wish.")
        }

        println()

        if (magicLamp.rubsLeft == 0 && lastGenie is RecyclableDemon){
            magicLamp.feedDemon(lastGenie)
            println("Fed the demon back into the lamp.\n")
        }

    }

}


