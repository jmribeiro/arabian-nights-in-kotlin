package arabiannights

class GrumpyGenie(maxWishes: Int): Genie(maxWishes){
    override fun canGrantWish() = grantedWishes == 0
}