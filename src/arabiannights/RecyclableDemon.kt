package arabiannights

class RecyclableDemon(maxWishes: Int): Genie(maxWishes){

    private var recycled = false

    fun recycle() {
        recycled = true
    }

    override fun canGrantWish() = !recycled

}