package arabiannights

abstract class Genie(val maxWishes: Int){

    var grantedWishes = 0
        private set

    fun grantWish(): Boolean {
        return if (maxWishes > 0 && canGrantWish()){
            grantedWishes++
            true
        }else{
            false
        }
    }

    val name: String = javaClass.simpleName

    abstract fun canGrantWish(): Boolean

}