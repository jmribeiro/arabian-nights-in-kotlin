package arabiannights

class FriendlyGenie(maxWishes: Int): Genie(maxWishes){
    override fun canGrantWish() = grantedWishes < maxWishes
}