package arabiannights

class MagicLamp(val maxRubs: Int){

    var rubsLeft = maxRubs
        private set

    var timesRecycled = 0
        private set

    fun rub(numWishes: Int): Genie {
        if (rubsLeft == 0) return RecyclableDemon(numWishes)
        val totalRubs = maxRubs - rubsLeft
        val evenRubs = totalRubs % 2 == 0
        rubsLeft -= 1
        return if (evenRubs) FriendlyGenie(numWishes)
        else GrumpyGenie(numWishes)
    }

    fun feedDemon(recyclableDemon: RecyclableDemon){
        rubsLeft = maxRubs
        timesRecycled++
        recyclableDemon.recycle()
    }

}
